﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WCFClientEnWPF.AirportServiceReference;

namespace WCFClientEnWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AirportServiceClient proxy;
        private Vol[] vols;
        private Bagage[] bagages;

        public MainWindow()
        {
            proxy = new AirportServiceClient();
            //proxy.ClientCredentials.UserName.UserName = "??";
            //proxy.ClientCredentials.UserName.Password = "??";
            InitializeComponent();
        }

        private void getVols_bt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ligne_tb_vol.Text != "" && cie_tb_vol.Text != "")
                {
                    vols = proxy.GetVolsByCieAndLineAndDates(ligne_tb_vol.Text, cie_tb_vol.Text, dhc_min.SelectedDate.GetValueOrDefault(), dhc_max.SelectedDate.GetValueOrDefault());
                }
                else if (dhc_min.SelectedDate.HasValue && dhc_max.SelectedDate.HasValue)
                {
                    vols = proxy.GetVolsByDates(dhc_min.SelectedDate.GetValueOrDefault(),
                        dhc_max.SelectedDate.GetValueOrDefault());
                }
                else
                {
                    vols = proxy.GetAllVols();
                }

                if (vols.Count() != 0 && vols != null)
                {
                    displayer_vols.Items.Clear();
                    displayer_vols.Items.Add("Id");
                    displayer_vols.Items.Add("Compagnie");
                    displayer_vols.Items.Add("Ligne");
                    displayer_vols.Items.Add("Jour Exploitation");
                    displayer_vols.Items.Add("Dernier Horaire");
                    displayer_vols.Items.Add("Statut");
                    displayer_vols.Items.Add("Parking");
                    displayer_vols.Items.Add("Type Avion");
                    displayer_vols.Items.Add("Immatriculation");
                    displayer_vols.Items.Add("Origine Création");

                    foreach (Vol v in vols)
                    {
                        displayer_vols.Items.Add(v.IdVol);
                        displayer_vols.Items.Add(v.Compagnie);
                        displayer_vols.Items.Add(v.Ligne);
                        displayer_vols.Items.Add(v.JourExploitation);
                        displayer_vols.Items.Add(v.DernierHoraire);
                        displayer_vols.Items.Add(v.Statut);
                        displayer_vols.Items.Add(v.Parking);
                        displayer_vols.Items.Add(v.TypeAvion);
                        displayer_vols.Items.Add(v.Immatriculation);
                        displayer_vols.Items.Add(v.OrigineCreation);
                    }
                }
                else
                {
                    displayer_vols.Items.Clear();
                    displayer_vols.Items.Add("Aucun vols ne correspondent aux critères de recherche");
                }
            }
            catch (Exception ex)
            {
                //connexion serveur impossible
                MessageBoxResult message = MessageBox.Show(this, ex.Message);
            }
        }

        private void getDetailVol_bt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (id_vol_tb.Text != "")
                {
                    int idVol;
                    if (int.TryParse(id_vol_tb.Text, out idVol))
                    {
                        WindowVol winVol = new WindowVol(proxy, idVol);
                        winVol.Show();
                    }
                    else
                    {
                        displayer_vols.Items.Clear();
                        displayer_vols.Items.Add("Veuillez entrer un ID valide");
                    }
                }
                else
                {
                    displayer_vols.Items.Clear();
                    displayer_vols.Items.Add("Veuillez entrer un ID valide");
                }
            }
            catch (Exception ex)
            {
                //connexion serveur impossible
                MessageBoxResult message = MessageBox.Show(this, ex.Message);
            }
        }

        private void addVol_bt_Click(object sender, RoutedEventArgs e)
        {
            WindowVol winVol = new WindowVol(proxy);
            winVol.SetCreateMode();
            winVol.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void getDetailBagage_bt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (id_bagage_tb.Text != "")
                {
                    int idBagage;
                    if (int.TryParse(id_bagage_tb.Text, out idBagage))
                    {
                        WindowBagage winBagage = new WindowBagage(proxy, idBagage);
                        winBagage.Show();
                    }
                    else
                    {
                        displayer_bagages.Items.Clear();
                        displayer_bagages.Items.Add("Veuillez entrer un ID valide");
                    }
                }
                else
                {
                    displayer_bagages.Items.Clear();
                    displayer_bagages.Items.Add("Veuillez entrer un ID valide");
                }
            }
            catch (Exception ex)
            {
                //connexion serveur impossible
                MessageBoxResult message = MessageBox.Show(this, ex.Message);
            }
        }

        private void getBagages_bt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (iata_tb.Text != "")
                {
                    bagages = proxy.GetBagagesByCodeIata(iata_tb.Text, dhc_min.SelectedDate.GetValueOrDefault(), dhc_max.SelectedDate.GetValueOrDefault());
                }
                else if (cie_tb_bagage.Text != "" && ligne_tb_bagage.Text != "")
                {
                    bagages = proxy.GetBagages(cie_tb_bagage.Text, ligne_tb_bagage.Text, dhc_min.SelectedDate.GetValueOrDefault(),
                        dhc_max.SelectedDate.GetValueOrDefault());
                }
                else
                {
                    displayer_bagages.Items.Clear();
                    displayer_bagages.Items.Add("Veuillez entrer des dates et: un code ligne et un code compagine ou un code iata");
                }

                if (bagages != null && bagages.Count() != 0)
                {
                    displayer_bagages.Items.Clear();
                    displayer_bagages.Items.Add("ID");
                    displayer_bagages.Items.Add("Code IATA");
                    displayer_bagages.Items.Add("Date Création");
                    displayer_bagages.Items.Add("Date Modification");
                    displayer_bagages.Items.Add("Vol ID");

                    foreach (Bagage b in bagages)
                    {
                        displayer_bagages.Items.Add(b.IdBagage);
                        displayer_bagages.Items.Add(b.CodeIata);
                        displayer_bagages.Items.Add(b.DateCreation);
                        displayer_bagages.Items.Add(b.DateModification);
                        displayer_bagages.Items.Add(b.Vol.IdVol);
                    }
                }
                else
                {
                    displayer_vols.Items.Clear();
                    displayer_vols.Items.Add("Aucun vols ne correspondent aux critères de recherche");
                }
            }
            catch (Exception ex)
            {
                //connexion serveur impossible
                MessageBoxResult message = MessageBox.Show(this, ex.Message);
            }
        }
    }
}
