﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WCFClientEnWPF.AirportServiceReference;

namespace WCFClientEnWPF
{
    /// <summary>
    /// Interaction logic for WindowBagage.xaml
    /// </summary>
    public partial class WindowBagage : Window
    {
        private AirportServiceClient proxy;
        private Bagage bagage;
        private Trace[] traces;

        public WindowBagage(AirportServiceClient proxy)
        {
            InitializeComponent();
            this.proxy = proxy;
        }

        public WindowBagage(AirportServiceClient proxy, int idBagage)
        {
            InitializeComponent();
            this.proxy = proxy;

            bagage = proxy.GetDetailBagage(idBagage);
            traces = proxy.GetBagageTrace(idBagage);

            if (bagage.Vol != null)
            {
                idvol_tb.Text = bagage.Vol.IdVol.ToString();
                cie_tb.Text = bagage.Vol.Compagnie;
                lig_tb.Text = bagage.Vol.Ligne;
            }
            idbagage_tb.Text = bagage.IdBagage.ToString();
            iata_tb.Text = bagage.CodeIata;
            crea_lb_2.Content = bagage.DateCreation.ToString();
            if (bagage.DateModification != null)
            {
                modif_lb_2.Content = bagage.DateModification.ToString();
            }
            else modif_lb.Visibility = Visibility.Collapsed;

            if (traces != null && traces.Count() != 0)
            {
                displayer_traces.Items.Clear();
                displayer_traces.Items.Add("Id");
                displayer_traces.Items.Add("IdBagage");
                displayer_traces.Items.Add("Horodate");
                displayer_traces.Items.Add("Type");
                displayer_traces.Items.Add("Localisation");
                displayer_traces.Items.Add("Information");
                displayer_traces.Items.Add("Statut");
                foreach (var t in traces)
                {
                    displayer_traces.Items.Add(t.IdTrace);
                    displayer_traces.Items.Add(t.IdBagage);
                    displayer_traces.Items.Add(t.Horodate);
                    displayer_traces.Items.Add(t.Type);
                    displayer_traces.Items.Add(t.Localisation);
                    displayer_traces.Items.Add(t.Information);
                    displayer_traces.Items.Add(t.Statut);
                }
            }
        }

        public void SetCreateMode()
        {
            validate_button.Click -= update;
            validate_button.Click += create;
            validate_button.Content = "Create";

            dismiss_bt.Click -= delete;
            dismiss_bt.Click += dismiss;
            dismiss_bt.Content = "Dismiss";

            idvol_tb.IsEnabled = false;
        }

        private void update(object sender, RoutedEventArgs e)
        {

        }

        private void create(object sender, RoutedEventArgs e)
        {
            if (iata_tb.Text != "" && idvol_tb.Text != "" && lig_tb.Text != "" && cie_tb.Text != "")
            {
                int idVol;
                if (int.TryParse(idbagage_tb.Text, out idVol))
                {
                    proxy.CreateBagage(iata_tb.Text, DateTime.Now, idVol, cie_tb.Text, lig_tb.Text);
                }
                else
                {
                    displayer_traces.Items.Clear();
                    displayer_traces.Items.Add("Erreur de parsing sur ID Bagage");
                }
            }
        }

        private void delete(object sender, RoutedEventArgs e)
        {
            int idBagage;
            if (int.TryParse(idbagage_tb.Text, out idBagage))
            {
                proxy.DeleteBagageById(idBagage);
                this.Close();
            }
            else
            {
                displayer_traces.Items.Clear();
                displayer_traces.Items.Add("Erreur de parsing sur ID Bagage");
            }
        }

        private void dismiss(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
