﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WCFClientEnWPF.AirportServiceReference;

namespace WCFClientEnWPF
{
    /// <summary>
    /// Interaction logic for WindowVol.xaml
    /// </summary>
    public partial class WindowVol : Window
    {
        private AirportServiceClient proxy;
        private Vol vol;
        private Bagage[] bagages;

        public WindowVol(AirportServiceClient proxy)
        {
            InitializeComponent();
            this.proxy = proxy;
        }

        public WindowVol(AirportServiceClient proxy, int idVol)
        {
            InitializeComponent();
            this.proxy = proxy;

            vol = proxy.GetDetailsVol(idVol);
            bagages = proxy.GetBagagesByIdVol(idVol);

            idvol_tb.Text = vol.IdVol.ToString();
            cie_tb.Text = vol.Compagnie;
            ligne_tb.Text = vol.Ligne;
            pkg_tb.Text = vol.Parking;
            jex_tb.Text = vol.JourExploitation.ToString();
            statut_tb.Text = vol.Statut.ToString();
            type_tb.Text = vol.TypeAvion;
            imm_tb.Text = vol.Immatriculation;
            dhc_tb.Text = vol.DernierHoraire.ToString(CultureInfo.InvariantCulture);

            if (bagages != null && bagages.Count() != 0)
            {
                displayer_bagages.Items.Clear();
                displayer_bagages.Items.Add("Id");
                displayer_bagages.Items.Add("Code IATA");
                displayer_bagages.Items.Add("Date de création");
                displayer_bagages.Items.Add("Date de modification");
                foreach (var b in bagages)
                {
                    displayer_bagages.Items.Add(b.IdBagage);
                    displayer_bagages.Items.Add(b.CodeIata);
                    displayer_bagages.Items.Add(b.DateCreation);
                    displayer_bagages.Items.Add(b.DateModification);
                }
            }
        }

        public void SetCreateMode()
        {
            validate_button.Click -= update;
            validate_button.Click += create;
            validate_button.Content = "Create";

            dismiss_bt.Click -= delete;
            dismiss_bt.Click += dismiss;
            dismiss_bt.Content = "Dismiss";

            idvol_tb.IsEnabled = false;
        }

        private void update(object sender, RoutedEventArgs e)
        {

        }

        private void create(object sender, RoutedEventArgs e)
        {
            proxy.CreateVol(cie_tb.Text, ligne_tb.Text, int.Parse(jex_tb.Text), statut_tb.Text, imm_tb.Text, pkg_tb.Text, dhc_dp.SelectedDate.GetValueOrDefault(), 'A', type_tb.Text);
        }

        private void delete(object sender, RoutedEventArgs e)
        {

        }

        private void dismiss(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
